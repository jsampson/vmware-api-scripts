These scripts do some pretty cool things with the VMWare API using pyvmomi that aren't typically done with the VMWare example scripts.

Written during my time at a large multi-national corporation with a very large VMWare environment, whose name will not be mentioned.