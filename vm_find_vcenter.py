#!/usr/bin/env python3
"""
Dev: Jeff Sampson
Date: 3-27-18
Description: Searches an offline pickle database for vCenter VMs.
Requirements to run:
    - Python 3.6 and all modules listed as 'import'
    - A pre-generated vCenter database.  This can be obtained by running
      vm_find_vcenter_offline_generate_db.py.
    - Connectivity to http://cmdlnx01094p04.corp.unnamedco.com is required for the
      initial run so it can download the database file.
"""
import pickle
import gzip
import os
import requests
import shutil
import datetime
import pathlib
import sys


# this is the location of our pickle file.  Don't change this.
PICKLE_FILE = os.environ['HOME'] + "/.config/vmdb/vmdb.pklz"
# this URL points to our database file
PICKLE_URL = "http://cmdlnx01094p04.corp.unnamedco.com/vmdb/vmdb.pklz"
# this is the pam_passwords file in JSON format.. you shouldn't don't touch this since the file is generated internally
PAM_PASSWORDS = os.environ['HOME'] + "/.pam_passwords.json"


def file_exists(file):
    """
    Looks for ./file  Returns True or False
    """
    if os.path.exists(file):
        return True
    return False


def read_pickle_file(pickle_file):
    """
    Reads a pickle file from pwd and returns an object
    """
    try:
        with gzip.open(pickle_file, 'rb') as f:
            p = pickle.load(f)
    except Exception:
        raise RuntimeError(f"Something went wrong reading from {pickle_file}")
    return(p)


def check_connectivity(url):
    """
    Does a very basic connection check to url
    """
    try:
        requests.get(url, timeout=1)
    except Exception:
        return False
    return True


def download_file(url, file_name):
    """
    Downloads a file from url and saves it to file_name
    """
    # grab the directory path from file_name
    file_dir = file_name.replace("/" + file_name.split("/")[-1], '')

    # create output directory if needed
    if not os.path.isdir(file_dir):
        pathlib.Path(file_dir).mkdir(parents=True, exist_ok=True)

    # do the actual download
    file = requests.get(url, stream=True)
    dump = file.raw
    with open(file_name, 'wb') as outfile:
        shutil.copyfileobj(dump, outfile)


def file_is_stale(file_name, expire_hours):
    """
    Checks if the timestamp of file_name is older than expire_hours
    """
    file_time_stamp = datetime.datetime.fromtimestamp(os.path.getmtime(file_name))
    current_time_stamp = datetime.datetime.now()
    expired_time_stamp = file_time_stamp + datetime.timedelta(hours=expire_hours)
    if current_time_stamp > expired_time_stamp:
        return True
    return False


def print_usage():
    """
    Prints a simple usage statement.
    """
    print("Error: you must supply an argument\n"
          " \n"
          "usage: vm_find_vcenter <vm>")
    sys.exit(1)


def get_args():
    """
    Grabs program argument and returns it
    """
    if len(sys.argv) < 2:
        print_usage()
    else:
        return str(sys.argv[1]).lower()


def main():
    """
    There can be only one.
    """

    # check program args
    vm = get_args()

    # Linux terminal color codes, taste the rainbow.
    color = {"header": '\033[95m',
             "okblue": '\033[94m',
             "okgreen": '\033[92m',
             "warning": '\033[93m',
             "fail": '\033[91m',
             "end": '\033[0m',
             "bold": '\033[1m',
             "underline": '\033[4m'}

    # it's ugly but it works.. update the database file
    if file_exists(PICKLE_FILE) and file_is_stale(PICKLE_FILE, 24) and check_connectivity(PICKLE_URL) \
       or not file_exists(PICKLE_FILE) and check_connectivity(PICKLE_URL):
        print(f"{color['bold']}Downloading new database file.{color['end']}")
        download_file(PICKLE_URL, PICKLE_FILE)

    # read database
    vcdb = read_pickle_file(PICKLE_FILE)

    # search the database for the VM
    print(f"{color['bold']}Searching vCenter database for VM {vm}.{color['end']}")
    try:
        if vcdb[vm.lower()]["hostname"]:
            print(f"{color['okgreen']}Found VM {vm} in the database.{color['end']}")
    except KeyError:
        print(f"{color['fail']}Error: could not find {vm} in the database.{color['end']}")
        sys.exit(1)

    # iterate through the vm's dict and print stuff out
    for k, v in vcdb[vm].items():
        print(f"{k}: {v}")


if __name__ == "__main__":
    main()
