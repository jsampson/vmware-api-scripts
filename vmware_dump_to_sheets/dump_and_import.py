#!/usr/bin/env python3
"""
Dev: Jeff Sampson
Date: 3-15-18
Description: Dumps all unnamedco vCenter data to csv files named for each environment
Requirements to run:
    - Python 3.x
    - vc_55_dump_vms_tofile.py in pwd.
    - vc_65_dump_vms_tofile.py in pwd.
    - import_csv_to_drive.py in pwd.
    - All of the prerequisites to run the 3 above scripts.  See each one for
      details.

Todo:
    - use multiprocessing to speed up dumping data from VCenter
    - remove .csv files after run
"""
from subprocess import call

# we're using 3 vmware service accounts to run these queries.  first field is
# the username and second is the password.
PROD_USER = ("svcaccountprod@corp.unnamedco.com", "password")
NON_PROD_USER = ("svcaccountnp@corp.unnamedco.com", "password")
ROBO_SVC_USER = ("svcaccountrobo@corp.unnamedco.com", "password")

# format: [user account flag, vcenter version, vcenter server hostname, output csv file]
VMWARE_HOSTS = [["rs", "65", "whsvcs00000p01.corp.unnamedco.com", "prod-vcenter1.csv"],
                ["rs", "65", "whsvcs00000p02.corp.unnamedco.com", "prod-vcenter2.csv"],
                ["rs", "65", "whsvcs00000p03.corp.unnamedco.com", "prod-vcenter3.csv"],
                ["rs", "65", "whsvcs00000p04.corp.unnamedco.com", "prod-vcenter4.csv"],
                ["rs", "65", "whsvcs00000p05.corp.unnamedco.com", "prod-vcenter5.csv"],
                ["rs", "65", "whsvcs00000p06.corp.unnamedco.com", "prod-vcenter6.csv"],
                ["rs", "65", "whsvcs00000p07.corp.unnamedco.com", "prod-vcenter7.csv"],
                ["rs", "65", "whsvcs00000p08.corp.unnamedco.com", "prod-vcenter8.csv"],
                ["rs", "65", "whsvcs00000p09.corp.unnamedco.com", "prod-vcenter9.csv"],
                ["rs", "65", "lrbovcs00000p01.corp.unnamedco.com", "prod-vcenterrf.csv"],
                ["p", "65", "vbkvcs01094p01.corp.unnamedco.com", "prod-SAP.csv"],
                ["np", "65", "wapvcs00099t01.corp.unnamedco.com", "np-lab-spp.csv"],
                ["np", "65", "np-vcsa.corp.unnamedco.com", "np-vcsa.csv"],
                ["p", "65", "appvcs01094p01.corp.unnamedco.com", "prod-wdcqdc.csv"],
                ["np", "65", "whsvcs00000t01.corp.unnamedco.com", "np-lab-vcenter.csv"]]


def main():
    """ This is main, duh """
    for item in VMWARE_HOSTS:
        # make things easier to read
        ver = item[1]
        vcenter_hostname = item[2]
        csv_file = item[3]

        if item[0] == "p":
            user = PROD_USER[0]
            pw = PROD_USER[1]
        elif item[0] == "np":
            user = NON_PROD_USER[0]
            pw = NON_PROD_USER[1]
        elif item[0] == "rs":
            user = ROBO_SVC_USER[0]
            pw = ROBO_SVC_USER[1]
        else:
            raise Exception('Please use a valid user account flag')

        print("Exporting VMs on " + vcenter_hostname
              + " to " + csv_file + ".  Please wait.")
        dump_cmd = "./vc_" + ver + "_dump_vms_tofile.py"
        # dump data
        call([dump_cmd, "-s", vcenter_hostname,
              "-u", user, "-p", pw, "-f", csv_file])
        print("Done.")

    # import csv files to google Sheets
    call(["./import_csv_to_sheets.py"])


# Start program
if __name__ == "__main__":
    main()
