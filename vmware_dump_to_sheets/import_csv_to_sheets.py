#!/usr/bin/env python3
"""
Dev: Jeff Sampson
Date: 3-15-18
Description: Dumps all unnamedco vCenter data to csv files named for each environment
Requirements to run:
    - Python 3.x
    - pygsheets python module
    - a Google Sheets API credentials file in JSON information
"""

import pygsheets
import csv
import glob

# you can find this in the URL for the sheet you want to update
SHEET_ID = "<put something here>"


def main():
    """ This is main, duh """
    # check pwd for csv files and add them to a list called csv_files
    csv_files = glob.glob('*.csv')
    if not csv_files:
        raise Exception("Error: No csv files found in current directory.")

    # connect to google drive.  make sure you have client_secret.json in the
    # current directory.  See this URL for details on how to set things up.
    # https://pygsheets.readthedocs.io/en/latest/authorizing.html
    print("Authenticating with the Google Sheets API.")
    try:
        gc = pygsheets.authorize()
    except Exception:
        raise Exception("Error: Cannot connect to Google Sheets.")

    for csv_name in csv_files:
        print("Reading csv data from file {0}".format(csv_name))
        with open(csv_name, "r") as f:
            reader = csv.reader(f)
            csv_file = list(reader)
        print("Importing {0}.".format(csv_name))
        # open the spreadsheet by ID -- this seems to be the most reliable
        sh = gc.open_by_key(SHEET_ID)

        # create a new sheet in the spreadsheet if one doesn't already exist
        try:
            wks = sh.worksheet_by_title(csv_name[:-4])
        except Exception:
            wks = sh.add_worksheet(csv_name[:-4], rows=1, cols=1)

        # update all cells starting at A1
        wks.update_cells('A1', csv_file, extend=True)


if __name__ == "__main__":
    main()
