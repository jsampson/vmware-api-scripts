#!/usr/bin/env python3
"""
Dev: Jeff Sampson
Date: 3-15-18
Description: Python program for dumping all VMs to a CSV file.  Based on the example python scripts that come with pyVmomi from VMWare.
Requirements to run:
    - Python 3.x
    - vCenter 6.5
    - pyVmomi 6.5.0.2017.5.1 (available from https://github.com/vmware/pyvmomi/tree/v6.5.0.2017.5-1)
    - At least read-only access to all VM objects in VCenter.
"""

from pyVim.connect import SmartConnect, Disconnect
from pyVmomi import vim

import argparse
import atexit
import getpass
import ssl


def GetArgs():
    """
    Supports the command-line arguments listed below.
    """
    parser = argparse.ArgumentParser(
        description='Process args for retrieving all the Virtual Machines')
    parser.add_argument('-s', '--host', required=True, action='store',
                        help='Remote host to connect to')
    parser.add_argument('-o', '--port', type=int, default=443, action='store',
                        help='Port to connect on')
    parser.add_argument('-u', '--user', required=True, action='store',
                        help='User name to use when connecting to host')
    parser.add_argument('-p', '--password', required=False, action='store',
                        help='Password to use when connecting to host')
    parser.add_argument('-f', '--file', required=True, action='store',
                        help='Name of file to write CSV output to')
    args = parser.parse_args()
    return args


def WriteVmInfoToFile(vm, outfile, depth=1):
    """
    Print information for a particular virtual machine or recurse into a folder
    or vApp with depth protection
    """
    maxdepth = 10

    # if this is a group it will have children. if it does, recurse into them
    # and then return
    if hasattr(vm, 'childEntity'):
        if depth > maxdepth:
            return
        vmList = vm.childEntity
        for c in vmList:
            WriteVmInfoToFile(c, outfile, depth + 1)
        return

    # if this is a vApp, it likely contains child VMs
    # (vApps can nest vApps, but it is hardly a common usecase, so ignore that)
    if isinstance(vm, vim.VirtualApp):
        vmList = vm.vm
        for c in vmList:
            WriteVmInfoToFile(c, outfile, depth + 1)
        return

    # write line to csv file
    summary = vm.summary
    with open(outfile, "a") as f:
        try:
            f.write("{0},{1},{2},{3},{4},{5},{6},{7},{8}\n".format(
                str(summary.config.name), str(summary.guest.hostName),
                str(summary.config.guestFullName), str(summary.runtime.powerState),
                str(summary.guest.ipAddress), str(summary.runtime.host.name),
                str(summary.config.vmPathName), str(summary.config.numCpu),
                str(summary.config.memorySizeMB)))
        except Exception:
            f.write("{0},{1},{2},{3},{4},{5},{6},{7},{8}\n".format(
                str(summary.config.name), str(summary.guest.hostName),
                str(summary.config.guestFullName), str(summary.runtime.powerState),
                str(summary.guest.ipAddress), "Error",
                str(summary.config.vmPathName), str(summary.config.numCpu),
                str(summary.config.memorySizeMB)))


def main():
    """
    Simple command-line program for listing the virtual machines on a system.
    """

    import os

    args = GetArgs()
    if args.password:
        password = args.password
    else:
        password = getpass.getpass(prompt='Enter password for host %s and '
                                          'user %s: ' % (args.host, args.user))

    context = None
    if hasattr(ssl, '_create_unverified_context'):
        context = ssl._create_unverified_context()
    si = SmartConnect(host=args.host,
                      user=args.user,
                      pwd=password,
                      port=int(args.port),
                      sslContext=context)
    if not si:
        print("Could not connect to the specified host using specified "
              "username and password")
        return -1

    atexit.register(Disconnect, si)

    # prep our output file
    outfile = args.file
    try:
        os.remove(outfile)
    except OSError:
        pass

    # write header to file
    with open(outfile, "a") as f:
        f.write(
            "name,hostName,guestFullName,powerState,ipAddress,vmHost,vmPathName,numCpu,memorySizeMB\n")

    # start iterating through vcenter
    content = si.RetrieveContent()
    for child in content.rootFolder.childEntity:
        if hasattr(child, 'vmFolder'):
            datacenter = child
            vmFolder = datacenter.vmFolder
            vmList = vmFolder.childEntity
            for vm in vmList:
                WriteVmInfoToFile(vm, outfile)
    return 0


# Start program
if __name__ == "__main__":
    main()
