#!/usr/bin/env python3
"""
Dev: Jeff Sampson
Date: 5-3-2018
Description: Python program for expanding an existing disk on a VM.  This
             version only works on VMs that are powered off since it tries
             to extend the vmdk file.
Requirements to run:
    - A powered off VM.
    - Python 3.x
    - vCenter 5.5 or higher?
    - pyVmomi (pip3 install pyVmomi)
    - Access to all relevant VM objects in vCenter.
"""

from pyVim.connect import SmartConnect, Disconnect
from pyVmomi import vim
import argparse
import getpass
import ssl
import atexit
import time


def main():
    """
    The main function.  There can be only one.
    """
    # gather command line arguments
    args = get_args()

    # connect to vcenter and automatically close the connection when done
    vc = vcenter_connect(args.host, args.user, args.password, int(args.port))
    atexit.register(Disconnect, vc)

    print('Searching for VM {0}..'.format(args.vm))
    vm_object = vcenter_get_dc_object(vc.RetrieveContent(), [vim.VirtualMachine], args.vm)
    # search_index = vc.content.searchIndex
    # vm_object = search_index.FindByDnsName(None, args.vm, True)
    if not vm_object:
        raise RuntimeError("Error: VM {0} not found.".format(args.vm))
    print('Extending Hard Disk {0} on VM {1}..'.format(args.disk_number, args.vm))
    vm_extend_disk(vc, vm_object, args.disk_number, mb_to_kb(args.newsize))
    print('Extend completed successfully.')


def mb_to_kb(mb):
    """
    Converts MB to Kb and returns the result
    """
    return mb * 1000


def vm_extend_disk(vc, vm_object, disk_number, totalsize):
    """
    This function extends a VM's disk
    """
    disk_label = "Hard disk " + disk_number
    disk_device_name = None
    dc = get_vm_datacenter(vm_object)
    for dev in vm_object.config.hardware.device:
        if isinstance(dev, vim.vm.device.VirtualDisk) and \
           dev.deviceInfo.label == disk_label:
            disk_device_name = dev.backing.fileName
    if not disk_device_name:
        raise RuntimeError('{0} could not be found.'.format(disk_label))

    # after we have found the disk device, do the expansion
    vdm = vc.content.virtualDiskManager
    task = vdm.ExtendVirtualDisk(
            disk_device_name, dc, totalsize, False)
    wait_for_task(task)


def wait_for_task(task, actionName='job', hideResult=False):
    """
    Waits and provides updates on a vCenter task.
    """
    while task.info.state == vim.TaskInfo.State.running:
        time.sleep(2)

    if task.info.state != vim.TaskInfo.State.success:
        raise Exception('%s did not complete successfully: %s' % (
                        actionName, task.info.error))


def vcenter_connect(hostname, username, password, prt):
    """
    Connects to vCenter and returns a SmartConnect object
    """
    context = None
    if hasattr(ssl, '_create_unverified_context'):
        context = ssl._create_unverified_context()
    si = SmartConnect(host=hostname,
                      user=username,
                      pwd=password,
                      port=int(prt),
                      sslContext=context)
    if not si:
        raise RuntimeError("Could not connect to vCenter using "
                           "the specified username and password")
    else:
        return si


def vcenter_get_dc_object(content, vim_type, name):
    """
    Retrieves object data from vCenter needed to manipulate the VM
    """
    obj = None
    container = content.viewManager.CreateContainerView(
        content.rootFolder, vim_type, True)
    for c in container.view:
        if c.name == name:
            obj = c
            break
    return obj


def get_vm_datacenter(obj):
    """
    Retrieves VM's folder, dc and root parent object by traversing linked
     list of parents back to beginning.
    """
    datacenter = 'N/A'

    if obj.parent:
        obj_parent = obj.parent
        while obj_parent:
            # Datacenter is the latest parent
            if isinstance(obj_parent, vim.Datacenter):
                datacenter = obj_parent
                break
            obj_parent = obj_parent.parent
    return (datacenter)


def get_args():
    """
    Gets command line arguments using the argparse python library
    """
    parser = argparse.ArgumentParser(
       description='Process args for retrieving all the Virtual Machines')
    parser.add_argument('-s', '--host',
                        required=True,
                        action='store',
                        help='Remote host to connect to')
    parser.add_argument('-o', '--port',
                        type=int,
                        default=443,
                        action='store',
                        help='Port to connect on')
    parser.add_argument('-u', '--user',
                        required=True,
                        action='store',
                        help='User name to use when connecting to host')
    parser.add_argument('-p', '--password',
                        required=False,
                        action='store',
                        help='Password to use when connecting to host')
    parser.add_argument('-v', '--vm',
                        required=True,
                        action='store',
                        help='VirtualMachine name')
    parser.add_argument('-d', '--disk_number',
                        required=True,
                        action='store',
                        help='VM disk number you want to extend.  eg: if the '
                             'disk is called "Hard Disk 3" you would enter 3')
    parser.add_argument('-n', '--newsize',
                        type=int,
                        required=True,
                        action='store',
                        help='New size (in MB) of the extended disk')
    args = parser.parse_args()

    # if user hasn't provided a password, prompt them for one.
    if not args.password:
        password = getpass.getpass(
         prompt='Enter password for host {0} and user {1}: '.format(
          args.host, args.user))
        args.password = password
    return args


# Start program
if __name__ == "__main__":
    main()
