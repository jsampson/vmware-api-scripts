#!/usr/bin/env python3
"""
Dev: Jeff Sampson
Date: 5-3-2018
Description: Python program for online expanding an existing disk on a VM.
Requirements to run:
    - Python 3.x
    - vCenter 5.5 or higher?
    - pyVmomi (pip3 install pyVmomi)
    - Access to all relevant VM objects in vCenter.
"""

from pyVim.connect import SmartConnect, Disconnect
from pyVim.task import WaitForTask
from pyVmomi import vim
import argparse
import ssl
import atexit
import pickle
import gzip
import os
import json
import subprocess
import requests
import shutil
import datetime
import pathlib
import sys


# this is the location of our pickle file.  Don't change this.
PICKLE_FILE = os.environ['HOME'] + "/.config/vmdb/vmdb.pklz"
# this URL points to our database file
PICKLE_URL = "http://cmdlnx01094p04.corp.unnamedco.com/vmdb/vmdb.pklz"
# this is the pam_passwords file in JSON format.. you shouldn't don't touch this since the file is generated internally
PAM_PASSWORDS = os.environ['HOME'] + "/.pam_passwords.json"


def main():
    """
    The main function.  There can be only one.
    """
    # gather command line arguments
    args = get_args()

    # Linux terminal color codes, taste the rainbow.
    color = {"header": '\033[95m',
             "okblue": '\033[94m',
             "okgreen": '\033[92m',
             "warning": '\033[93m',
             "fail": '\033[91m',
             "end": '\033[0m',
             "bold": '\033[1m',
             "underline": '\033[4m'}

    # check for stale/non-existent database file
    if file_exists(PICKLE_FILE) and file_is_stale(PICKLE_FILE, 24) and check_connectivity(PICKLE_URL) \
       or not file_exists(PICKLE_FILE) and check_connectivity(PICKLE_URL):
        print(f"{color['bold']}Downloading new database file.{color['end']}")
        download_file(PICKLE_URL, PICKLE_FILE)

    # check for stale/non-existent ~/.pam_passwords.json file
    if file_exists(PAM_PASSWORDS) and file_is_stale(PAM_PASSWORDS, 8) and check_connectivity(PICKLE_URL) \
       or not file_exists(PAM_PASSWORDS) and check_connectivity(PICKLE_URL):
        print(f"{color['bold']}Updating PAM passwords.  Please wait.{color['end']}")
        update_pam_passwords()

    # read database file, which is in pickle format
    vcdb = read_pickle_file(PICKLE_FILE)

    # search the database for the VM
    print(f"{color['bold']}Searching vCenter offline database for VM {args.vm}.{color['end']}")
    try:
        if vcdb[args.vm.lower()]["hostname"]:
            print(
                f"{color['okgreen']}Found VM {args.vm} in the database.{color['end']}")
    except KeyError:
        print(
            f"{color['fail']}Error: could not find {args.vm} in the database.{color['end']}")
        sys.exit(1)

    # read pam password json file and get username/pass for our corp.unnamedco.com account
    passwords = read_pam_passwords(PAM_PASSWORDS)
    user = get_corp_user(passwords)
    pw = get_corp_password(passwords)

    # connect to vcenter and automatically close the connection when done
    vc = vcenter_connect(vcdb[args.vm]['vcenter'], user, pw)
    atexit.register(Disconnect, vc)

    print(f"{color['bold']}Searching vCenter for VM {args.vm}..{color['end']}")
    search_index = vc.content.searchIndex
    vm = search_index.FindByDnsName(None, args.vm, True)
    if not vm:
        raise RuntimeError(
            f"{color['fail']}VM {args.vm} not found.{color['end']}")
    print(f"{color['bold']}Extending Hard Disk {args.disk_number} on VM {args.vm} "
          f"to {args.newsizeingigabytes} GB..{color['end']}")
    vm_extend_disk(vm, args.disk_number, gb_to_kb(args.newsizeingigabytes))
    print(f"{color['okgreen']}Completed successfully.{color['end']}")


def check_connectivity(url):
    """
    Does a very basic connection check to url
    """
    try:
        requests.get(url, timeout=1)
    except Exception:
        return False
    return True


def file_exists(file):
    """
    Looks for ./file  Returns True or False
    """
    if os.path.exists(file):
        return True
    return False


def download_file(url, file_name):
    """
    Downloads a file from url and saves it to file_name
    """
    # grab the directory path from file_name
    file_dir = file_name.replace("/" + file_name.split("/")[-1], '')

    # create output directory if needed
    if not os.path.isdir(file_dir):
        pathlib.Path(file_dir).mkdir(parents=True, exist_ok=True)

    # do the actual download
    file = requests.get(url, stream=True)
    dump = file.raw
    with open(file_name, 'wb') as outfile:
        shutil.copyfileobj(dump, outfile)


def read_pam_passwords(pw_json_file):
    """
    Reads a pre-populated json file containing PAM usernames and passwords and returns it as a dictionary.
    """
    with open(pw_json_file, 'r') as f:
        try:
            pam_passwords = json.loads(f.read())
        except Exception:
            raise RuntimeError(f"Something went wrong reading from {pw_json_file}.  "
                               f"Try deleting it and running this script again.")

    # convert json list to a python dict and return it
    pam_dict = {}
    for i in pam_passwords:
        pam_dict.update({i['Account']: i['Password']})
    return pam_dict


def get_corp_user(pam_passwords):
    """
    Grabs our corp.unnamedco.com -a account from a dictionary
    """
    for k in pam_passwords:
        if "-a@corp.unnamedco.com" in k:
            return k


def get_corp_password(pam_passwords):
    """
    Grabs our corp.unnamedco.com -a account password from a dictionary
    """
    for k in pam_passwords:
        if "-a@corp.unnamedco.com" in k:
            return pam_passwords[k]


def update_pam_passwords():
    """
    Runs the pamctl command to update the user's passwords file
    """
    try:
        proc = subprocess.Popen(["pamctl", "retrieve", "favorites", "--no-status", "-o", "json"],
                                stdout=subprocess.PIPE)
    except Exception:
        raise RuntimeError(
            "Error: Something went wrong running 'pamctl retrieve favorites'.")

    with open(PAM_PASSWORDS, 'wb') as out_file:
        out_file.write(proc.stdout.read())


def file_is_stale(file_name, expire_hours):
    """
    Checks if the timestamp of file_name is older than expire_hours
    """
    file_time_stamp = datetime.datetime.fromtimestamp(
        os.path.getmtime(file_name))
    current_time_stamp = datetime.datetime.now()
    expired_time_stamp = file_time_stamp + \
        datetime.timedelta(hours=expire_hours)
    if current_time_stamp > expired_time_stamp:
        return True
    return False


def read_pickle_file(pickle_file):
    """
    Reads a pickle file from pwd and returns an object
    """
    try:
        with gzip.open(pickle_file, 'rb') as f:
            p = pickle.load(f)
    except Exception:
        raise RuntimeError(f"Something went wrong reading from {pickle_file}")
    return(p)


def gb_to_kb(gb):
    """
    Converts Gigabytes to Kilobytes and returns the result
    """
    return gb * 1024 * 1024


def vm_extend_disk(vm, disk_number, new_size):
    """
    This function extends a VM's disk
    """
    disk_label = "Hard disk " + disk_number
    disk = None
    # iterate through VM disks until we find the right one
    for dev in vm.config.hardware.device:
        if isinstance(dev, vim.vm.device.VirtualDisk) and \
           dev.deviceInfo.label == disk_label:
            disk = dev
            break
    if not disk:
        raise RuntimeError(f'{disk_label} could not be found.')

    # after we have found the disk device, do the expansion
    disk.capacityInKB = new_size
    spec = vim.vm.ConfigSpec()
    devSpec = vim.vm.device.VirtualDeviceSpec(device=disk, operation="edit")
    spec.deviceChange.append(devSpec)
    WaitForTask(vm.Reconfigure(spec))


def vcenter_connect(hostname, username, password, prt=443):
    """
    Connects to vCenter and returns a SmartConnect object
    """
    context = None
    if hasattr(ssl, '_create_unverified_context'):
        context = ssl._create_unverified_context()
    si = SmartConnect(host=hostname,
                      user=username,
                      pwd=password,
                      port=int(prt),
                      sslContext=context)
    if not si:
        raise RuntimeError("Could not connect to vCenter using "
                           "the specified username and password")
    else:
        return si


def vcenter_get_dc_object(content, vim_type, name):
    """
    Retrieves object data from vCenter needed to manipulate the VM
    """
    obj = None
    container = content.viewManager.CreateContainerView(
        content.rootFolder, vim_type, True)
    for c in container.view:
        if c.name == name:
            obj = c
            break
    return obj


def get_args():
    """
    Gets command line arguments using the argparse python library
    """
    parser = argparse.ArgumentParser(
        description='Process args for retrieving all the Virtual Machines')
    parser.add_argument('-v', '--vm',
                        required=True,
                        action='store',
                        help='VirtualMachine name')
    parser.add_argument('-d', '--disk_number',
                        required=True,
                        action='store',
                        help='VM disk number you want to extend.  eg: if the '
                             'disk is called "Hard Disk 3" you would enter 3')
    parser.add_argument('-n', '--newsizeingigabytes',
                        type=int,
                        required=True,
                        action='store',
                        help='New size (in GB) of the extended disk')
    args = parser.parse_args()
    return args


# Start program
if __name__ == "__main__":
    main()
