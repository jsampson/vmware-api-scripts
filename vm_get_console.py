#!/usr/bin/env python3
"""
Dev: Jeff Sampson
Date: 7-27-18
Description: Searches the offline pickle database for vCenter VMs and uses VMWare
             Remote Console to establish a console session with a VM.
Requirements to run:
    - Python 3.6 and all modules listed next to 'import'
    - The VMWare Remote Console application must be installed.  Go here for the software and instructions:
        https://drive.google.com/drive/folders/1Or0wkfER0IN15fUfVlljcHQcMysJSDdW
    - You must be using pam_utilities.  pam_utilities is located here:
        https://gitlab.corp.unnamedco.com/IAM/tooling/pam-utilities
    - Connectivity to the unnamedco Intranet is required for the initial run so it can download the
      vCenter database file and your PAM passwords.
"""

import argparse
import pickle
import gzip
import os
import json
import subprocess
import requests
import shutil
import datetime
import pathlib
import sys


# this is the location of our pickle file.  Don't change this.
PICKLE_FILE = os.environ['HOME'] + "/.config/vmdb/vmdb.pklz"
# this URL points to our database file.  Don't change this either.
PICKLE_URL = "http://cmdlnx01094p04.corp.unnamedco.com/vmdb/vmdb.pklz"
# this is the pam_passwords file in JSON format.. you shouldn't don't touch this since the file is generated internally
PAM_PASSWORDS = os.environ['HOME'] + "/.pam_passwords.json"


def get_args():
    """
    Gets command line arguments using the argparse python library
    """
    parser = argparse.ArgumentParser(
       description='Process args for retrieving Virtual Machine console information')
    parser.add_argument('-v', '--vm',
                        required=True,
                        action='store',
                        help='VirtualMachine name')
    args = parser.parse_args()
    return args


def check_connectivity(url):
    """
    Does a very basic connection check to url
    """
    try:
        requests.get(url, timeout=1)
    except Exception:
        return False
    return True


def file_exists(file):
    """
    Looks for ./file  Returns True or False
    """
    if os.path.exists(file):
        return True
    return False


def download_file(url, file_name):
    """
    Downloads a file from url and saves it to file_name
    """
    # grab the directory path from file_name
    file_dir = file_name.replace("/" + file_name.split("/")[-1], '')

    # create output directory if needed
    if not os.path.isdir(file_dir):
        pathlib.Path(file_dir).mkdir(parents=True, exist_ok=True)

    # do the actual download
    file = requests.get(url, stream=True)
    dump = file.raw
    with open(file_name, 'wb') as outfile:
        shutil.copyfileobj(dump, outfile)


def read_pam_passwords(pw_json_file):
    """
    Reads a pre-populated json file containing PAM usernames and passwords and returns it as a dictionary.
    """
    with open(pw_json_file, 'r') as f:
        try:
            pam_passwords = json.loads(f.read())
        except Exception:
            raise RuntimeError(f"Something went wrong reading from {pw_json_file}.  "
                               f"Try deleting it and running this script again.")

    # convert json list to a python dict and return it
    pam_dict = {}
    for i in pam_passwords:
        pam_dict.update({i['Account']: i['Password']})
    return pam_dict


def get_corp_user(pam_passwords):
    """
    Grabs our corp.unnamedco.com -a account from a dictionary
    """
    for k in pam_passwords:
        if "-a@corp.unnamedco.com" in k:
            return k


def get_corp_password(pam_passwords):
    """
    Grabs our corp.unnamedco.com -a account password from a dictionary
    """
    for k in pam_passwords:
        if "-a@corp.unnamedco.com" in k:
            return pam_passwords[k]


def update_pam_passwords():
    """
    Runs the pamctl command to update the user's passwords file
    """
    try:
        proc = subprocess.Popen(["pamctl", "retrieve", "favorites", "--no-status", "-o", "json"],
                                stdout=subprocess.PIPE)
    except Exception:
        raise RuntimeError("Error: Something went wrong running 'pamctl retrieve favorites'.")

    with open(PAM_PASSWORDS, 'wb') as out_file:
        out_file.write(proc.stdout.read())


def file_is_stale(file_name, expire_hours):
    """
    Checks if the timestamp of file_name is older than expire_hours
    """
    file_time_stamp = datetime.datetime.fromtimestamp(os.path.getmtime(file_name))
    current_time_stamp = datetime.datetime.now()
    expired_time_stamp = file_time_stamp + datetime.timedelta(hours=expire_hours)
    if current_time_stamp > expired_time_stamp:
        return True
    return False


def read_pickle_file(pickle_file):
    """
    Reads a pickle file from pwd and returns an object
    """
    try:
        with gzip.open(pickle_file, 'rb') as f:
            p = pickle.load(f)
    except Exception:
        raise RuntimeError(f"Something went wrong reading from {pickle_file}")
    return(p)


def print_usage():
    """
    Prints a simple usage statement.
    """
    print("Error: you must supply an argument\n"
          " \n"
          "usage: vm_get_console <vm>")
    sys.exit(1)


def get_arg():
    """
    Grabs program argument and returns it
    """
    if len(sys.argv) < 2:
        print_usage()
    else:
        return str(sys.argv[1]).lower()


def main():
    """
    There can be only one.
    """

    # check program args
    vm = get_arg()

    # Linux terminal color codes, taste the rainbow.
    color = {"header": '\033[95m',
             "okblue": '\033[94m',
             "okgreen": '\033[92m',
             "warning": '\033[93m',
             "fail": '\033[91m',
             "end": '\033[0m',
             "bold": '\033[1m',
             "underline": '\033[4m'}

    # check for stale/non-existent database file
    if file_exists(PICKLE_FILE) and file_is_stale(PICKLE_FILE, 24) and check_connectivity(PICKLE_URL) \
       or not file_exists(PICKLE_FILE) and check_connectivity(PICKLE_URL):
        print(f"{color['bold']}Downloading new database file.{color['end']}")
        download_file(PICKLE_URL, PICKLE_FILE)

    # read database file, which is in pickle format
    vcdb = read_pickle_file(PICKLE_FILE)

    # search the database for the VM
    print(f"{color['bold']}Searching vCenter offline database file ({PICKLE_FILE}) for VM {vm}.{color['end']}")
    try:
        if vcdb[vm.lower()]["hostname"]:
            print(f"{color['okgreen']}Found VM {vm} in the database.{color['end']}")
    except KeyError:
        print(f"{color['fail']}Error: could not find {vm} in the database.{color['end']}")
        sys.exit(1)

    # check for stale/non-existent ~/.pam_passwords.json file
    if file_exists(PAM_PASSWORDS) and file_is_stale(PAM_PASSWORDS, 8) \
       or not file_exists(PAM_PASSWORDS) and check_connectivity(PICKLE_URL):
        print(f"{color['bold']}Updating PAM passwords.  Please wait.{color['end']}")
        update_pam_passwords()

    # read pam password json file and get username/pass for our corp.unnamedco.com account
    passwords = read_pam_passwords(PAM_PASSWORDS)
    user = get_corp_user(passwords)
    pw = get_corp_password(passwords)

    # launch VMRC session
    print(f"{color['okgreen']}Launching VMRC session to {vm} on {vcdb[vm]['vcenter']}.{color['end']}")
    try:
        subprocess.Popen(['vmrc', '-M', vcdb[vm]['moId'],
                                  '-U', user,
                                  '-P', pw,
                                  '-H', vcdb[vm]['vcenter']])
    except FileNotFoundError:
        print("Could not find the vmrc application.  You did read the instructions at the top of this script right?")
        sys.exit(1)


if __name__ == "__main__":
    main()
