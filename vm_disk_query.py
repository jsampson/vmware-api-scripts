#!/usr/bin/env python3
"""
Dev: Jeff Sampson
Date: 5-3-2018
Description: Python program for gathering disk info from a VM.
Credits: Portions shamelessly stolen from virtual_machine_device_info.py from
         the pyVmomi community scripts.
Requirements to run:
    - Python 3.x
    - vCenter 5.5 or higher?
    - jslib
    - Access to all relevant VM objects in vCenter.
"""
from jslib import ph
from jslib import obj as o
from jslib import pklh as pk
from pyVim.connect import SmartConnect, Disconnect
from pyVmomi import vim
import sys
import os
import ssl
import atexit
import math
import click


@click.command()
@click.argument('vm', nargs=1)
@click.argument('disk_number', nargs=1)
def main(vm, disk_number):
    """
    The main function.  There can be only one.
    """

    pickle_file = os.environ['HOME'] + "/.config/vmdb/vmdb.pklz"
    pickle_url = "http://cmdlnx01094p04.corp.unnamedco.com/vmdb/vmdb.pklz"
    pam_file = os.environ['HOME'] + "/.pam_passwords.json"
    pam_url = "https://pam.unnamedco.com/"

    # Linux terminal color codes, taste the rainbow.
    color = {"header": '\033[95m',
             "okblue": '\033[94m',
             "okgreen": '\033[92m',
             "warning": '\033[93m',
             "fail": '\033[91m',
             "end": '\033[0m',
             "bold": '\033[1m',
             "underline": '\033[4m'}

    # update vCenter db if needed
    if o.update_needed(pickle_file, 24) and o.check_connectivity(pickle_url):
        print(f"{color['bold']}Downloading new database file.{color['end']}")
        o.download_file(pickle_url, pickle_file)
    elif not o.update_needed(pickle_file, 24):
        pass
    else:
        print(f"{color['fail']}Update server unavailable.  Connect to the unnamedco LAN and try again.{color['end']}")
        sys.exit(1)

    # update ~/.pam_file.json if needed
    if o.update_needed(pam_file, 4) and o.check_connectivity(pam_url):
        print(f"{color['bold']}Updating PAM passwords.  Please wait.{color['end']}")
        ph.update_pam_passwords(pam_file)
    elif not o.update_needed(pam_file, 4):
        pass
    else:
        print('failed')
        sys.exit(1)

    # read vCenter database file, which is in pickle format
    vcdb = pk.read_pickle_file(pickle_file)

    # search the database for the VM
    print(f"{color['bold']}Searching vCenter offline database for VM {vm}.{color['end']}")
    try:
        if vcdb[vm.lower()]["hostname"]:
            print(f"{color['okgreen']}Found VM {vm} in the database.{color['end']}")
    except KeyError:
        print(f"{color['fail']}Error: could not find {vm} in the database.{color['end']}")
        sys.exit(1)

    # read pam password json file
    try:
        passwords = ph.read_pam_passwords(pam_file)
    except FileNotFoundError:
        print(f"{color['bold']}Updating PAM passwords.  Please wait.{color['end']}")
        ph.update_pam_passwords(pam_file)
        passwords = ph.read_pam_passwords(pam_file)
    except Exception:
        print(f"{color['fail']}Error: something went wrong reading the pam passwords file.{color['end']}")
        sys.exit(1)
    user = ph.get_corp_user(passwords)
    pw = ph.get_corp_password(passwords)

    # connect to vcenter and automatically close the connection when done
    vc = vcenter_connect(vcdb[vm]['vcenter'], user, pw)
    atexit.register(Disconnect, vc)

    print(f"Connecting to vCenter {vcdb[vm]['vcenter']}..")
    # vm_object = vcenter_get_dc_object(vc.RetrieveContent(), [vim.VirtualMachine], vm)
    search_index = vc.content.searchIndex
    vm_object = search_index.FindByDnsName(None, vm, True)
    if not vm_object:
        raise RuntimeError("Error: VM {vm} not found.")
    print(f'Querying Hard Disk(s) on VM {vm}..\n')
    vm_query_disk(vm_object, disk_number)
    print('Query completed successfully.')


def vm_query_disk(vm_object, disk_number):
    """
    This function queries a VM for disk data
    """
    disk_label = "Hard disk " + disk_number

    if disk_number != "a":
        for dev in vm_object.config.hardware.device:
            if isinstance(dev, vim.vm.device.VirtualDisk) and \
               dev.deviceInfo.label == disk_label:
                datastore = dev.backing.datastore
                # convert dev.deviceInfo.summary from KB to bytes for convert_size()
                summary_size = convert_size(int(dev.deviceInfo.summary[:-3].replace(',', '')) * 1024)
                print(f"disk label: {dev.deviceInfo.label}\n"
                      f"size: {summary_size}\n"
                      f"device type: {type(dev).__name__}\n"
                      f"backing type: {type(dev.backing).__name__}\n"
                      f"datastore disk size: {convert_size(datastore.summary.capacity)}\n"
                      f"datastore disk used: {convert_size(datastore.summary.capacity - datastore.summary.freeSpace)}\n"
                      f"datastore disk free: {convert_size(datastore.summary.freeSpace)}\n")
    elif disk_number == "a":
        for dev in vm_object.config.hardware.device:
            if isinstance(dev, vim.vm.device.VirtualDisk):
                summary_size = convert_size(int(dev.deviceInfo.summary[:-3].replace(',', '')) * 1024)
                datastore = dev.backing.datastore
                print(f"disk label: {dev.deviceInfo.label}\n"
                      f"size: {summary_size}\n"
                      f"device type: {type(dev).__name__}\n"
                      f"backing type: {type(dev.backing).__name__}\n"
                      f"file name: {dev.backing.fileName}\n"
                      f"datastore disk size: {convert_size(datastore.summary.capacity)}\n"
                      f"datastore disk used: {convert_size(datastore.summary.capacity - datastore.summary.freeSpace)}\n"
                      f"datastore disk free: {convert_size(datastore.summary.freeSpace)}\n")
    else:
        raise RuntimeError(f"You provided an invalid disk number '{disk_number}'")


def vcenter_connect(hostname, username, password, prt=443):
    """
    Connects to vCenter and returns a SmartConnect object
    """
    context = None
    if hasattr(ssl, '_create_unverified_context'):
        context = ssl._create_unverified_context()
    si = SmartConnect(host=hostname,
                      user=username,
                      pwd=password,
                      port=int(prt),
                      sslContext=context)
    if not si:
        raise RuntimeError("Could not connect to vCenter using "
                           "the specified username and password")
    else:
        return si


def vcenter_get_dc_object(content, vim_type, name):
    """
    Retrieves object data from vCenter needed to manipulate the VM
    """
    obj = None
    container = content.viewManager.CreateContainerView(
        content.rootFolder, vim_type, True)
    for c in container.view:
        if c.name == name:
            obj = c
            break
    return obj


def convert_size(size_bytes):
    if size_bytes == 0:
        return "0B"
    size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    i = int(math.floor(math.log(size_bytes, 1024)))
    p = math.pow(1024, i)
    s = round(size_bytes / p, 2)
    return "%s %s" % (s, size_name[i])


# Start program
if __name__ == "__main__":
    main()
