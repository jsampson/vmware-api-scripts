#!/bin/bash

# this script installs all of the things needed to run my vmware automation setuptools

#
# functions first..
#
pip_configure () {
  # make sure there isn't an existing pip config file
  if [ -f /home/$SUDO_USER/.config/pip/pip.conf ]; then
    echo "An existing pip config file was found [/home/$SUDO_USER/.config/pip/pip.conf].  Please remove it and rerun this script."
    exit 1
  fi
  # do the stuff
  su - $SUDO_USER -c 'mkdir -p ~/.config/pip'
  su - $SUDO_USER -c 'echo "[global]" >> ~/.config/pip/pip.conf'
  su - $SUDO_USER -c 'echo "user = True" >> ~/.config/pip/pip.conf'
  su - $SUDO_USER -c 'echo "cert = /etc/ssl/certs/ca-bundle.crt" >> ~/.config/pip/pip.conf'
}

unnamedco_ca_check () {
  grep -q unnamedco /etc/ssl/certs/ca-bundle.crt
  if [ $? -ne 0 ]; then
    echo "Could not find unnamedco CA Certificates in /etc/ssl/certs/ca-bundle.crt.  Please install them and rerun this script.  Exiting."
    exit 1
  fi
}

dnf_install_if_missing () {
  echo -n "Checking for $1..  "
  rpm -q $1 --quiet
  if [ "$?" != "0" ]; then
    echo "Installing: $1."
    dnf install $1 -y
  else
    echo "Skipping install: $1."
  fi
}

install_jslib () {
  su - $SUDO_USER -c 'pip3 install jslib --user --extra-index-url http://cmdlnx01094p04.corp.unnamedco.com/pypi/simple --trusted-host cmdlnx01094p04.corp.unnamedco.com'
}

install_vmrc() {
  # just FYI Fedora 28 requires ncurses-compat-libs before the installer will run
  echo "Downloading VMWare Remote Console.."
  curl http://cmdlnx01094p04.corp.unnamedco.com/vmrc/VMware-Remote-Console-10.0.3-9300449.x86_64.bundle -o vmrc.bundle
  echo "Done."

  if [ ! -f "./vmrc.bundle" ]; then
    echo "Something went wrong downloading the VMWare Remote Console.  Exiting."
  fi

  echo "Installing VMWare Remote Console.."
  chmod +x ./vmrc.bundle
  # this installs using the console only and skips any input questions
  ./vmrc.bundle --eulas-agreed --console --required
  rm -f ./vmrc.bundle
  echo "Done."
}

#
# below is the actual script stuff
#

# make sure we're root
if [ `whoami` != "root" ]; then
  echo "You must run this script as root.  Exiting."
  exit 1
fi

# make sure we were called with sudo
if [ ! $SUDO_USER ]; then
  echo "Please run this script using the sudo command (either sudo -i or sudo ./script.sh will work)"
  exit 1
fi

echo "Checking unnamedco CA trust."
unnamedco_ca_check

# make sure we're at python 3.6
echo "Installing prerequisites."
dnf_install_if_missing "python3"
dnf_install_if_missing "chromedriver"
dnf_install_if_missing "stoken-cli"
dnf_install_if_missing "ncurses-compat-libs"

echo "Configuring pip for the unnamedco environment."
pip_configure

echo "Installing Python modules."
install_jslib

echo "Installing VMWare Remote Console."
install_vmrc

echo "All installations completed successfully.. now it's time to go RTFM."

cat << "EOF"
 _______________________________________
/ Cow says: More steps are required to  \
\ use this software.  Please see below. /
 ---------------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||

You will need to complete the following steps:

Step 1. You will need to set up stoken so that pamctl can use it.  If you need instructions on how to do this, follow along:

  [10:56] jsampson@jslaptop ~ $ stoken import --file=jeffsampson_000150541264.sdtid <the file here comes from an email you received when you first set up your RSA token.  This should be for your unnamedco LAN -a account.>
  Enter new password: <press enter here>
  Confirm new password: <press enter here>
  [10:56] jsampson@jslaptop ~ $ stoken setpin
  Enter new PIN: <enter your RSA pin here>
  Confirm new PIN: <enter your RSA pin here>

Step 2. Verify that you are getting the correct RSA token from stoken.  Compare the number to your RSA token mobile app.

  [10:56] jsampson@jslaptop ~ $ stoken
  41581078 <this number perfectly matches what I see in the mobile app>

Step 3. Ensure you have selected all of your passwords as Favorites in the PAM Website.

Step 4. Set up pamctl so that it can retrieve your passwords from the PAM website.

  [11:00] jsampson@jslaptop ~ $ pamctl auth setup
  Please enter your PAM username: jeffsampson <unnamedco LAN Account goes here>
  Please enter your PAM password: <unnamedco LAN Account password goes here>
  Would you like to use stoken to manage your RSA token? [Y/n]: y
  Do you use encryption with stoken? [Y/n]: n
  Please enter your RSA PIN: <enter your RSA pin here>
  Credentials successfully saved.

Step 5. Make sure pamctl is able to retrieve your PAM passwords.  This usually takes a minute.

  [11:00] jsampson@jslaptop ~ $ pamctl retrieve favorites
  Collecting passwords from favorites list, this could take a little while...
    [####################################]  100%

  Account                        Password
  -----------------------------  ----------------------
  jeffsampson-a@corp.unnamedco.com  ,7u1!-YivK@*0uw$#PT(Z_
  jeffsampson-a@np.unnamedco.com    %O6Nh*VRgb,D%FS[JmfmQe
  jeffsampson-z@np.unnamedco.com    QzBodh{Y%R+GM@EPvvh<fH
  jeffsampson-z@corp.unnamedco.com  2&CpxbkQpu0BOcP_%1vQhl

Step 6. Begin using Jeff's VMWare tools.  You can find all of them in the toolbox git repo in /scripts/python/vmware_api
Step 7. ?????????
Step 8. Profit!

EOF

# set up pamctl
# give instructions on how to finish setting things up

echo "Installation completed.  Exiting."
exit 0
